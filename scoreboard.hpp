#pragma once
#include <wx/scrolwin.h>
#include <wx/string.h>
#include <wx/window.h>
// The scoreboard displays players' scores in the GUI
class Scoreboard: public wxScrolledWindow {
public:
  Scoreboard(wxWindow*);
  void AddEntry(wxString&);
  void UpdateEntry(wxString&, int);
  void RemoveEntry(wxString&);
};

