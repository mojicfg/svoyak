#include <wx/button.h>
#include <wx/defs.h>
#include <wx/panel.h>
#include <wx/sizer.h>
#include <wx/textctrl.h>
#include <wx/valtext.h>
#include "const.hpp"
#include "dialog.hpp"

HostCreateDialog::HostCreateDialog() :
wxDialog(nullptr, wxID_ANY, L"Створити гру") {
  SetBackgroundColour(COL_MAIN);
  wxPanel *panel = new wxPanel(this, wxID_ANY);
  // The wxTextValidator restricts wxTextCtrl's character set.
  wxTextValidator numberTV(wxFILTER_INCLUDE_CHAR_LIST);
  numberTV.SetCharIncludes("0123456789");
  // Put everything into a vertical wxBoxSizer.
  wxBoxSizer *vbox = new wxBoxSizer(wxVERTICAL);
  panel->SetSizer(vbox);
  ShowModal();
  Destroy();
}
PlayerJoinDialog::PlayerJoinDialog() :
wxDialog(nullptr, wxID_ANY, L"Приєднатися до гри") {
  SetBackgroundColour(COL_MAIN);
  wxPanel *panel = new wxPanel(this, wxID_ANY);
  // The wxTextValidator restricts wxTextCtrl's character set.
  wxTextValidator numberTV(wxFILTER_INCLUDE_CHAR_LIST);
  numberTV.SetCharIncludes("0123456789");
  // Put everything into a vertical wxBoxSizer.
  wxBoxSizer *vbox = new wxBoxSizer(wxVERTICAL);
  panel->SetSizer(vbox);
  ShowModal();
  Destroy();
}

