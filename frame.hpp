#pragma once
#include <wx/event.h>
#include <wx/frame.h>
#include <wx/panel.h>
// The Frame is an app window, its implementation describes
// the GUI & menu bar layout. It is also responsible for
// catching events such as a key press.
class Frame: public wxFrame {
public:
  Frame();
private:
  wxPanel *inner;
  // Menubar-related events, i.e. menu selection.
  void OnHostMenuCreate(wxCommandEvent&);
  void OnPlayerMenuJoin(wxCommandEvent&);
  // Catching resize event to force aspect ratio.
  void OnSize(wxSizeEvent&);
  // Catching key press globally on a Frame level.
  // May be considered rather hacky, but whatever.
  void OnCharHook(wxKeyEvent&);
  DECLARE_EVENT_TABLE()
};

