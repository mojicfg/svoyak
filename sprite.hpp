#pragma once
#include <wx/bitmap.h>
#include <wx/dcclient.h>
#include <wx/image.h>
#include <wx/panel.h>
#include <functional>
// The Sprite class based on the wxPanel class
// provides an easy-to-use "texture panel"
// with custom drawing function
typedef std::function<void(wxDC&)> render_t;
class Sprite: public wxPanel {
public:
  wxImage image; // hack - public for lambda accessibility in SetRender()
  Sprite(wxWindow*, const wxString&, wxBitmapType = wxBITMAP_TYPE_PNG);
  void SetRender(render_t);
private:
  render_t render;
  void OnPaint(wxPaintEvent&);
  DECLARE_EVENT_TABLE()
};

