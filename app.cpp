#include <wx/app.h>
#include "frame.hpp"
// This is the wxWidgets base for a GUI application.
// Implementing only OnInit() is enough.
class App: public wxApp {
public:
  bool OnInit() {
    Frame *frame = new Frame();
    frame->Show();
    // no need to use delete,
    // wxWidgets will handle it properly
    return true; // return true upon successful init
  }
};
IMPLEMENT_APP(App); // main()-generating macro for the App class

