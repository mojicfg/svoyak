#pragma once
// The networking module provides some interfaces
// for the app's multiplayer functionality
enum {
  NETWORKING_SUCCESS,
  NETWORKING_ERROR,
};
int create_server(char* port);
int join_server(char* ip, char* port);
int submit_time(int time);

