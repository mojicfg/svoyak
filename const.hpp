#pragma once
#include <wx/defs.h> // wxID_*
#include <wx/colour.h> // wxColour
#include <wx/gdicmn.h> // wxSize
#include <chrono>
// IDs
enum {
  // IDs for the Host menu on the menu bar
  _HOSTMENU = wxID_HIGHEST,
  ID_HOSTMENU_CREATE = _HOSTMENU + 1,
  ID_HOSTMENU_OPTION = _HOSTMENU + 2,
  // IDs for the Player menu on the menu bar
  _PLAYERMENU = _HOSTMENU + 10,
  ID_PLAYERMENU_JOIN = _PLAYERMENU + 1,
};
// Networking
const char DEFAULT_PORT[] = "1337";
// GUI colour palette
const wxColour COL_MAIN = wxColour("#b5b6b8");
const wxColour COL_MAIN_DARKER = wxColour("#aaabad");
const wxColour COL_MAIN_DARK = wxColour("#88898b");
const wxColour COL_SEC = wxColour("#fcff34");
// Timings
const auto MONITOR_BLOCKED_INTERVAL =
  std::chrono::milliseconds(5000);
const auto MONITOR_ON_INTERVAL =
  std::chrono::milliseconds(5000);
// Frame settings
const int ASPECT_W = 16;
const int ASPECT_H = 9;
const int FRAME_MARGIN = 20;
const int DIALOG_MARGIN = FRAME_MARGIN;
const int FRAME_HBOX_L = 20;
const int FRAME_HBOX_M = 60;
const int FRAME_HBOX_R = 20;
const int FRAME_MBOX_U = 75;
const int FRAME_MBOX_D = 25;
const int FRAME_RBOX_U = 30;
const int FRAME_RBOX_M = 45;
const int FRAME_RBOX_D = 25;
// These are the "client size" limits (excluding the menu bar).
const wxSize FRAME_MIN_SIZE = wxSize(50 * ASPECT_W, 50 * ASPECT_H);
const wxSize FRAME_MAX_SIZE = wxSize(120 * ASPECT_W, 120 * ASPECT_H);
const wxSize FRAME_INIT_SIZE = FRAME_MIN_SIZE;

