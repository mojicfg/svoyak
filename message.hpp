#pragma once
#include <wx/panel.h>
#include <wx/string.h>
#include <wx/window.h>
// The Message class is essentially a wxPanel
// displaying the host's message in the GUI
class Message: public wxPanel {
public:
  Message(wxWindow*);
  void SetText(wxString&);
};

