#include <wx/scrolwin.h>
#include <wx/sizer.h>
#include <wx/textctrl.h>
#include "chat.hpp"
#include "const.hpp"
Chat::Chat(wxWindow *parent) : wxPanel(parent) {
  SetBackgroundColour(COL_MAIN_DARKER);
  wxScrolledWindow *scrollable = new wxScrolledWindow(this);
  // TODO: Set up a text validator.
  wxTextCtrl *tc = new wxTextCtrl(this, wxID_ANY);
  wxBoxSizer *box = new wxBoxSizer(wxVERTICAL);
  box->Add(scrollable, 1);
  box->Add(tc, 0, wxEXPAND);
  SetSizer(box);
}

