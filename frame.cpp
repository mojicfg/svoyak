#include <wx/menu.h>
#include <wx/panel.h>
#include <wx/sizer.h>
#include "chat.hpp"
#include "const.hpp"
#include "dialog.hpp"
#include "frame.hpp"
#include "message.hpp"
#include "monitor.hpp"
#include "scoreboard.hpp"
#include "sprite.hpp"
#include "status.hpp"

BEGIN_EVENT_TABLE(Frame, wxFrame)
  EVT_MENU(ID_HOSTMENU_CREATE, Frame::OnHostMenuCreate)
  EVT_MENU(ID_PLAYERMENU_JOIN, Frame::OnPlayerMenuJoin)
  EVT_SIZE(Frame::OnSize)
  EVT_CHAR_HOOK(Frame::OnCharHook)
END_EVENT_TABLE()

Frame::Frame() : wxFrame(nullptr, wxID_ANY, "wx app"), inner(nullptr) {
  // TODO: change title
  // Menu bar layout
  wxMenuBar *menubar = new wxMenuBar();
  wxMenu *playerMenu = new wxMenu();
  playerMenu->Append(ID_PLAYERMENU_JOIN, L"Приєднатися до гри");
  menubar->Append(playerMenu, L"Гравець");
  wxMenu *hostMenu = new wxMenu();
  hostMenu->Append(ID_HOSTMENU_CREATE, L"Створити гру");
  hostMenu->Append(ID_HOSTMENU_OPTION, L"Опції гри");
  menubar->Append(hostMenu, L"Відник");
  menubar->SetBackgroundColour(COL_MAIN_DARK);
  menubar->SetForegroundColour(COL_SEC);
  SetMenuBar(menubar);
  // Add client size constraints *AFTER* the menu bar.
  SetClientSize(FRAME_INIT_SIZE);
  SetMinClientSize(FRAME_MIN_SIZE);
  SetMaxClientSize(FRAME_MAX_SIZE);
  SetBackgroundColour(COL_MAIN);
  // Medieval aspect-ratio-forcing witchcraft
  wxPanel *outer = new wxPanel(this);
  outer->SetBackgroundColour(COL_MAIN);
  inner = new wxPanel(outer);
  inner->SetBackgroundColour(COL_MAIN);
  inner->SetSize(GetClientSize());
  // GUI elements
  Chat *chat = new Chat(inner);
  wxPanel *mPanel = new wxPanel(inner);
  Monitor *monitor = new Monitor(mPanel);
  Scoreboard *scoreboard = new Scoreboard(mPanel);
  wxPanel *rPanel = new wxPanel(inner);
  Message *message = new Message(rPanel);
  Sprite *host = new Sprite(rPanel, "assets/host0.png");
  host->SetBackgroundColour(COL_MAIN_DARKER);
  host->SetRender([host](wxDC &dc) {
    float scale = (float) dc.GetSize().x / (float) host->image.GetWidth();
    int scaledW = host->image.GetWidth() * scale;
    int scaledH = host->image.GetHeight() * scale;
    wxImage scaled = host->image.Scale(scaledW, scaledH,
        wxIMAGE_QUALITY_BOX_AVERAGE);
    dc.DrawBitmap(scaled,
        (dc.GetSize().x - scaledW),
        (dc.GetSize().y - scaledH));
  });
  Status *status = new Status(rPanel);
  // Put some onto the middle wxPanel.
  wxBoxSizer *mbox = new wxBoxSizer(wxVERTICAL);
  mbox->Add(monitor, FRAME_MBOX_U, wxEXPAND | wxDOWN, FRAME_MARGIN);
  mbox->Add(scoreboard, FRAME_MBOX_D, wxEXPAND, FRAME_MARGIN);
  mPanel->SetSizer(mbox);
  // Put some onto the right wxPanel.
  wxBoxSizer *rbox = new wxBoxSizer(wxVERTICAL);
  rbox->Add(message, FRAME_RBOX_U, wxEXPAND | wxDOWN, FRAME_MARGIN);
  rbox->Add(host, FRAME_RBOX_M, wxEXPAND | wxDOWN, FRAME_MARGIN);
  rbox->Add(status, FRAME_RBOX_D, wxEXPAND, FRAME_MARGIN);
  rPanel->SetSizer(rbox);
  // Put everything into a global wxBoxSizer.
  wxBoxSizer *hbox = new wxBoxSizer(wxHORIZONTAL);
  hbox->Add(chat, FRAME_HBOX_L, wxEXPAND | wxALL, FRAME_MARGIN);
  hbox->Add(mPanel, FRAME_HBOX_M, wxEXPAND | wxUP | wxDOWN, FRAME_MARGIN);
  hbox->Add(rPanel, FRAME_HBOX_R, wxEXPAND | wxALL, FRAME_MARGIN);
  inner->SetSizer(hbox);
  inner->Layout();
}
void Frame::OnHostMenuCreate(wxCommandEvent&) {
  HostCreateDialog();
}
void Frame::OnPlayerMenuJoin(wxCommandEvent&) {
  PlayerJoinDialog();
}
void Frame::OnSize(wxSizeEvent &ev) {
  // Ancient aspect-ratio-forcing magical spell
  const int frame_w = GetClientSize().GetX(),
            frame_h = GetClientSize().GetY();
  const int amt = std::min(frame_w / ASPECT_W,
                           frame_h / ASPECT_H);
  const int inner_w = amt * ASPECT_W,
            inner_h = amt * ASPECT_H;
  if (inner != nullptr) { // avoiding segfault on startup
    const int margin_w = (frame_w - inner_w) / 2;
    const int margin_h = (frame_h - inner_h) / 2;
    inner->SetSize(margin_w, margin_h, inner_w, inner_h);
  }
  // Skipping this event is necessary for sizers to work!
  ev.Skip();
}
void Frame::OnCharHook(wxKeyEvent &ev) {
  // Skipping this event is necessary for text ctrls to work!
  ev.Skip();
}

