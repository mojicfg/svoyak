#include "sprite.hpp"
BEGIN_EVENT_TABLE(Sprite, wxPanel)
  EVT_PAINT(Sprite::OnPaint)
END_EVENT_TABLE()
Sprite::Sprite(wxWindow *parent, const wxString &path,
    wxBitmapType format) : wxPanel(parent) {
  wxBitmap imageBMP;
  imageBMP.LoadFile(path, format);
  image = imageBMP.ConvertToImage();
}
void Sprite::SetRender(render_t render) {
  this->render = render;
}
void Sprite::OnPaint(wxPaintEvent&) {
  wxPaintDC dc(this);
  render(dc);
}

