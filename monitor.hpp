#pragma once
#include <wx/panel.h>
#include <wx/string.h>
#include <wx/window.h>
// The Monitor class is the central part of the GUI displaying the main
// game info.
// Currently, this class also handles the Ctrl key press and submits
// the time to the server.
// Displayed info mode passed to SetDisplay()
enum { 
  DISPLAY_LOGO,
  DISPLAY_GRID,
  DISPLAY_QUESTION,
};
class Monitor: public wxPanel {
public:
  Monitor(wxWindow*);
  // Methods updating displayed info.
  void SetDisplay(int);
  void UpdateGrid(/* TODO */);
  void UpdateQuestion(wxString&);
  // Methods handling key press.
  void TurnOn();
  void OnPress();
private:
  // Key press handling system's state
  enum {
    MONITOR_OFF,
    MONITOR_ON,
    MONITOR_BLOCKED,
  } state;
};

