CXX=g++
CXXFLAGS=-std=c++17 `wx-config --cxxflags`

app: app.o chat.o dialog.o frame.o message.o monitor.o scoreboard.o sprite.o status.o
	$(CXX) -oapp `wx-config --libs` $^

app.o: app.cpp frame.hpp
	$(CXX) -c $< $(CXXFLAGS)

chat.o: chat.cpp chat.hpp const.hpp
	$(CXX) -c $< $(CXXFLAGS)

dialog.o: dialog.cpp const.hpp dialog.hpp
	$(CXX) -c $< $(CXXFLAGS)

frame.o: frame.cpp chat.hpp const.hpp dialog.hpp frame.hpp message.hpp monitor.hpp scoreboard.hpp sprite.hpp status.hpp
	$(CXX) -c $< $(CXXFLAGS)

message.o: message.cpp const.hpp message.hpp
	$(CXX) -c $< $(CXXFLAGS)

monitor.o: monitor.cpp const.hpp monitor.hpp
	$(CXX) -c $< $(CXXFLAGS)

scoreboard.o: scoreboard.cpp const.hpp scoreboard.hpp
	$(CXX) -c $< $(CXXFLAGS)

sprite.o: sprite.cpp sprite.hpp
	$(CXX) -c $< $(CXXFLAGS)

status.o: status.cpp const.hpp status.hpp
	$(CXX) -c $< $(CXXFLAGS)

clean:
	rm -f *.o

